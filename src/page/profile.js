import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, Alert, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux'
import { Icon, Button, InputItem, WhiteSpace } from '@ant-design/react-native';
import NavigationBar from 'react-native-navbar';
import styles from '../utilities/styles'
import { push } from 'connected-react-router'
import axios from 'axios'

class profile extends Component {
  state = {
    email: '',
    firstName: '',
    lastName: '',
    pic: 'https://www.orthocaremedical.com/wp-content/uploads/person-icon.png'
  }
  UNSAFE_componentWillMount() {
    const { profile } = this.props
    console.log("profile:" + this.props.profile[profile.length - 1].email);
    var id = this.props.profile[profile.length - 1].id
    axios({
      method: 'get',
      url: `http://3.83.202.0:8888//user/${id}`
    })
      .then(res => {
        const { data } = res
        const { user } = data
        console.log("data.email :" + data.email);
        this.setState({ email: data.email })
        this.setState({ lastName: data.lastName })
        this.setState({ firstName: data.firstName })
      }).catch(error => {
        console.log("ERROR:", error);
      })

  }
  goToEditProfile = () => {
    this.props.push('/editProfile')
  }
  logOut = () => {
    this.props.push('/login')
    alert('Logout Success')
  }

  render() {
    const { profile } = this.props
    return (
      <View style={styles.container}>
        <View style={styles.header} >
          <View style={{ flex: 1, }}>
            <NavigationBar title={{ title: 'Profile' }} rightButton={{
              title: 'Logout',
              handler: () => this.logOut()
            }} />
          </View>
        </View>
        <WhiteSpace />
        <View style={{ flex: 2, alignItems: 'center' }}>
          <Image style={{ width: 150, height: 150, borderRadius: 150 / 2, backgroundColor: '#000' }}
            source={{ uri: this.state.pic }} />
          <WhiteSpace />
          <View style={{ width: 240, alignItems: 'center' }}>
            <Text style={styles.welcome}><Icon name={'mail'} size='xs' /> {this.state.email}</Text>
            <WhiteSpace />
            <Text style={styles.welcome}><Icon name={'user'} size='xs' />  {this.state.firstName}  {this.state.lastName}</Text>
            <WhiteSpace />
            <Button onPress={() => { this.goToEditProfile() }}>Edit Profile</Button>
            <WhiteSpace />
          </View>
        </View>
      </View>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    profile: state.profile
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    editProfile: (email, firstname, lastname) => {
      dispatch({
        type: 'EDIT_PROFILE',
        email: email,
        firstname: firstname,
        lastname: lastname
      })
    },
    push: location => {
      dispatch(push(location))
    },
    removeProfile: () => {
      dispatch({
        type: 'REMOVE_PROFILE',
        index: 0
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(profile)