import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, Alert, TextInput, TouchableOpacity, FlatList } from 'react-native';
import { connect } from 'react-redux'
import NavigationBar from 'react-native-navbar';
import { NavBar, Icon, Card, Button, WingBlank, WhiteSpace, Tabs } from '@ant-design/react-native';
import styles from '../utilities/styles'
import axios from 'axios'
class historyTicket extends Component {
  state = {
    tickets: [],
    ticket: []
  }
  UNSAFE_componentWillMount() {
    const { profile } = this.props
    var id = this.props.profile[profile.length - 1].id
    axios.get(`http://3.83.202.0:8888/booking/user/${id}`)
      .then(res => {
        console.log("Res:" + JSON.stringify(res.data[0].showtime.movie.name));
        this.setState({ tickets: res.data })
      })
      .catch(error => {
        console.log('ERROR:', error);
      })
  }
  seatPrint = (seats) => {
    const seat = []
    for (let index = 0; index < seats.length; index++) {
      seat.push(<Text>Seat: {seats[index].column}</Text>)
    }
    return seat
  }
  addZero(time) {
    if (time < 10) {
      time = "0" + time;
    }
    return time;
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={{ flex: 0.8 }}>
          <NavigationBar
            title={{
              title: 'Ticket',
            }}
          />
        </View>
        <View style={{ flex: 11 }}>
        <FlatList
            data={this.state.tickets}
            renderItem={({ item,index }) =>
              <Card>
                <Card.Header
                  title={item.showtime.movie.name}
                  extra={item.showtime.movie.duration + ' min'}
                />
                <Card.Body>
                  <View style={{ flexDirection: 'row' }}>
                    <View>
                      <WingBlank>
                        <TouchableOpacity>
                          <Image style={{ width: 150, height: 222 }}
                            source={{ uri: item.showtime.movie.image }} />
                        </TouchableOpacity>
                      </WingBlank>
                    </View>
                    <View>
                      <Text>{new Date(item.creatDateTime).toLocaleDateString()}</Text>
                      {console.log("item.seats: "+item.seats)}
                      {this.seatPrint(item.seats)}
                      <Text> Show time : {this.addZero(new Date(item.showtime.startDateTime).toLocaleDateString())} </Text>
                      <Text> {this.addZero(new Date(item.showtime.startDateTime).getHours())} : {this.addZero(new Date(item.showtime.startDateTime).getMinutes())}</Text>
                    </View>
                  </View>
                </Card.Body>
                <Card.Footer
                  content="Soundtrack / Subtitle"
                  extra={item.showtime.soundtrack + '/' + item.showtime.subtitles}
                />
              </Card>
            }
          />
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    profile: state.profile
  }
}
export default connect(mapStateToProps)(historyTicket)