import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { connect } from 'react-redux'
import { Icon, Button, InputItem, WhiteSpace } from '@ant-design/react-native';
import NavigationBar from 'react-native-navbar';
import styles from '../utilities/styles'
import axios from 'axios'

class register extends Component {

  state = {
    email: '',
    password: '',
    firstname: '',
    lastname: '',
    passwordConfirm: ''
  }
  goToLogin = () => {
    this.props.history.push('/login')
  }

  saveProfile = (email, password, firstname, lastname, passwordConfirm) => {
    if (email === '') {
      alert("Email can't be blank")
    } else if (firstname === ''){
      alert("First Name can't be blank")
    }
    else if (lastname === ''){
      alert("Last Name can't be blank")
    }
    else if (password === ''){
      alert("Password can't be blank")
    }
    else if (passwordConfirm === ''){
      alert("Password Confirm can't be blank")
    }
    else if (password !== passwordConfirm) {
      alert('Password does not match')
    } else {
      axios({
        url: 'http://3.83.202.0:8888/user/register',
        method: 'post',
        data: {
          firstName: firstname,
          lastName: lastname,
          password: password,
          email: email,
        }
      }).then(res => {
        const { data } = res
        const { user } = data
        this.props.history.push('/login')
        alert('Register Success')
      }).catch(e => {
        if (e.response.data.errors.email === "duplicated email") {
          alert('This email has already been used.')
        }
      })
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header} >
          <View style={{ flex: 1, }}>
            <NavigationBar title={{ title: 'Regsiter' }}
              leftButton={{
                title: 'Back',
                handler: () => this.goToLogin()
              }}
              statusBar={{ showAnimation: 'slide', tintColor: 'black' }} />
          </View>
        </View>
        <WhiteSpace />
        <View style={{ flex: 4, alignItems: 'center' }}>
          <View >
            <InputItem placeholder='Email'
              clear
              labelNumber={2}
              type="primary"
              style={{}}
              onChangeText={(value) => { this.setState({ email: value }) }} ><Icon name={'mail'} /></InputItem>
            <WhiteSpace />
            <InputItem placeholder='First name'
              clear
              labelNumber={2}
              type="primary"
              style={{}}
              onChangeText={(value) => { this.setState({ firstname: value }) }} ><Icon name={'user'} /></InputItem>
            <InputItem placeholder='Last name'
              clear
              labelNumber={2}
              type="primary"
              style={{}}
              onChangeText={(value) => { this.setState({ lastname: value }) }}> </InputItem>

            <WhiteSpace />
            <InputItem placeholder='Password'
              clear
              labelNumber={2}
              type="password"
              style={{ height: 40 }}
              onChangeText={(value) => { this.setState({ password: value }) }} ><Icon name={'lock'} /></InputItem>
            <InputItem placeholder='Confrim Password'
              clear
              labelNumber={2}
              type="password"
              style={{ height: 40 }}
              onChangeText={(value) => { this.setState({ passwordConfirm: value }) }} > </InputItem>
            <WhiteSpace />
            <Button onPress={() => { this.saveProfile(this.state.email, this.state.password, this.state.firstname, this.state.lastname, this.state.passwordConfirm) }}
              type="primary"
              style={{ width: 300, height: 40 }} >Register</Button>

            <WhiteSpace />
          </View>
        </View>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addProfile: (email, password, firstname, lastname) => {
      dispatch({
        type: 'ADD_PROFILE',
        email: email,
        password: password,
        firstname: firstname,
        lastname: lastname
      })
    }
  }
}

export default connect(null, mapDispatchToProps)(register)