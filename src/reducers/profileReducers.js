function profileReducer(state = [], action) {
  switch (action.type) {
    case 'ADD_PROFILE':
      return {
        email: action.email,
        password: action.password,
        firstname: action.firstname,
        lastname: action.lastname,
        id: action.id
      }
    case 'EDIT_PROFILE':
      return {
        ...state,
        ...action.payload
      }
    case 'REMOVE_PROFILE':
      return state.filter((item, index) => index !== action.index)

    default:
      return state
  }
}

function profilesReducer(state = [], action) {
  switch (action.type) {
    case 'ADD_PROFILE':
      return [...state, profileReducer(null, action)]
    case 'EDIT_PROFILE':
      return state.map((each, index) => {
        if (each.email === action.email) {
          return profileReducer(each, index)
        }
        return each
      })
    default:
      return state
  }
}

export default profilesReducer